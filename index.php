<?php

error_reporting(E_ALL);
ini_set('display_errors', 'on');
header("Content-Type: text/plain");
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Headers: Authorization");
header("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");


define("ROOT_DIR", implode('/', explode('\\', $_SERVER['CONTEXT_DOCUMENT_ROOT'] . '\\')));

function __autoload($class)
{
  //ehhh windows have case insensitive paths
  $path = explode('\\', $class);
  $className = array_pop($path);
  $path = strtolower(implode('/', $path)) . '/';

  require_once(ROOT_DIR . $path . $className . '.php');
}

use App\App;

require_once('./config.php');

$app = new App();
$app->run();