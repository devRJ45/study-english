<?php


namespace Core;


class Response
{
  private $closed = false;
  private $status = 200;
  private $data = "";

  public function isClosed ()
  {
    return $this->closed;
  }

  public function status ($code)
  {
    $this->status = $code;
    return $this;
  }

  public function write ($data)
  {
    $this->data .= $data;
  }

  public function end ($data)
  {
    $this->closed = true;
    $this->write($data);
    return true;
  }

  public function show ()
  {
    http_response_code($this->status);
    echo $this->data;
  }
}