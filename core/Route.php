<?php


namespace Core;


use App\ControllerLoader;
use App\MiddlewareLoader;

class Route
{
  private $method;
  private $path;
  private $stack = array();

  public function __construct(string $path, string $method)
  {
    $this->path = $path;
    $this->method = $method;
  }

  public function addToStack ($obj) {
    array_push($this->stack, $obj);
  }

  public function matchPath ($path, $method)
  {
    if ($this->method !== 'ALL' && $this->method !== $method)
      return false;

    $routePathSegments = explode('/', $this->path);
    $pathSegments = explode('/', $path);

    if (count($this->stack) == 1 && $this->stack[0] instanceof Router)
    {
      for ($i = 0; $i < count($routePathSegments); $i++)
      {
        if (!isset($pathSegments[$i]) || $pathSegments[$i] != $routePathSegments[$i])
            return false;
      }
      return true;
    }

    if (count($pathSegments) > count($routePathSegments))
      return false;

    for ($i = 0; $i < count($pathSegments); $i++)
    {
      if ($pathSegments[$i] != $routePathSegments[$i])
      {
        if (strlen(strval($routePathSegments[$i])) <= 0 || strval($routePathSegments[$i])[0] != ':')
          return false;
      }
    }
    return true;
  }

  private function assignParams (Request $req)
  {
    $routePathSegments = explode('/', $this->path);
    $pathSegments = explode('/', $req->path);

    if (count($pathSegments) > count($routePathSegments))
      return false;

    for ($i = 0; $i < count($pathSegments); $i++)
    {
      if ($pathSegments[$i] != $routePathSegments[$i])
      {
        if (strlen(strval($routePathSegments[$i])) > 0 || strval($routePathSegments[$i])[0] == ':')
        {
          $paramName = substr(strval($routePathSegments[$i]), 1);
          $req->params[$paramName] = $pathSegments[$i];
        }
      }
    }
    return true;
  }

  public function dispatch (Request $req, Response $res)
  {
    $this->assignParams($req);

    foreach ($this->stack as $obj)
    {
      if ($obj instanceof Router) //is router
      {
        $routePathSegments = explode('/', $this->path);
        $reqPathSegments = explode('/', $req->path);

        for ($i = 0; $i < count($routePathSegments); $i++)
        {
          array_shift($reqPathSegments);
        }

        $req->path = '/' . join('/', $reqPathSegments);

        $obj->handle($req, $res);
        return;
      }
      elseif ($obj) //is middleware or controller
      {
        if ($obj[0] === '#') //middleware
        {
          $middlewareObj = MiddlewareLoader::getMiddleware($obj);
          if ($middlewareObj == null)
          {
            echo("Cannot find middleware $obj");
            return;
          }
          $objSegments = explode('/', $obj);
          $method = end($objSegments);
          $middlewareObj->execute($method, $req, $res);

          if ($res->isClosed()) {
            return;
          }
        }
        elseif ($obj[0] === '@') //is controller
        {
          $controllerObj = ControllerLoader::getController($obj);
          if ($controllerObj == null)
          {
            echo("Cannot find controller $obj");
            return;
          }
          $objSegments = explode('/', $obj);
          $method = end($objSegments);
          $controllerObj->execute($method, $req, $res);

          if ($res->isClosed()) {
            return;
          }
        }
      }

      //debug only
//      var_dump($obj);
//      var_dump($req);
    }
  }
}