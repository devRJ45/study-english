<?php


namespace Core;


class Request
{
  public $host;
  public $method;
  public $originalPath;
  public $path;
  public $headers;
  public $query;
  public $postData;

  public $params = array();

  public function __construct($server, $headers, $get, $post)
  {
    $this->parseServerVariables($server, $headers, $get, $post);
  }

  private function parseServerVariables ($server, $headers, $get, $post)
  {
    $this->host = $_SERVER['HTTP_HOST'];
    $this->method = isset($server["REQUEST_METHOD"]) ? $server["REQUEST_METHOD"] : "GET";
    $this->originalPath = isset($server['REDIRECT_URL']) ? $this->parsePath($server["REDIRECT_URL"]) : $this->parsePath($server["REQUEST_URI"]);
    $this->path = $this->originalPath;
    $this->headers = $headers;
    $this->query = $get;
    $this->postData = $post;
  }

  private function parsePath ($path)
  {
    $path = explode('?', $path);
    $path = explode('/', $path[0]);

    $path = array_filter($path, function ($val) {
      return strlen($val) > 0;
    });

    $path = '/' . join('/', $path);

    return $path;
  }
}