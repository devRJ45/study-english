<?php


namespace Core;


interface RouterInterface
{
  public function handle (Request $req, Response $res);
  public function use(string $path, $middlewareOrRouter);
  public function get(string $path, array $middlewares, $controller);
  public function post(string $path, array $middlewares, $controller);
}