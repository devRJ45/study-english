<?php


namespace Core;


class AppAbstract implements RouterInterface
{
  protected $router;

  protected $request;
  protected $response;

  public function __construct()
  {
    $this->router = new namespace\Router();
    
    //decode axios request
    $post = $_POST;
    if (isset($_POST[array_key_first($_POST)]) && strlen($_POST[array_key_first($_POST)]) <= 0) {

      $post = json_decode(file_get_contents('php://input'), true);
      if (isset($post['body']))
        $post = $post['body'];
    }

    $this->request = new Request($_SERVER, getallheaders(), $_GET, $post);
    $this->response = new Response();

    $this->request->app = $this;
  }

  public function handle(Request $req, Response $res)
  {
    $this->router->handle($req, $res);
  }

  public function use (string $path, $middlewareOrRouter)
  {
    $this->router->use($path, $middlewareOrRouter);
  }

  public function get(string $path, $middlewares, $controller)
  {
    $this->router->get($path, $middlewares, $controller);
  }
  public function post(string $path, $middlewares, $controller)
  {
    $this->router->post($path, $middlewares, $controller);
  }

  public function run() {
    $this->router->handle($this->request, $this->response);
    $this->response->show();
  }

}