<?php


namespace Core;


use App\MiddlewareLoader;

class Router implements RouterInterface
{
  private $stack = array();
  private $routes = array();


  public function use(string $path, $middlewareOrRouter)
  {
    if ($middlewareOrRouter instanceof Router)
    {
      $route = new Route($path, "ALL");
      $route->addToStack($middlewareOrRouter);
      array_push($this->routes, $route);
    }
    else
    {
      array_push($this->stack, $middlewareOrRouter);
    }
  }

  public function get(string $path, array $middlewares, $controller)
  {
    $route = new Route($path, "GET");

    foreach ($middlewares as $middleware)
    {
      $route->addToStack($middleware);
    }
    $route->addToStack($controller);
    array_push($this->routes, $route);
  }

  public function post(string $path, array $middlewares, $controller)
  {
    $route = new Route($path, "POST");

    foreach ($middlewares as $middleware)
    {
      $route->addToStack($middleware);
    }
    $route->addToStack($controller);
    array_push($this->routes, $route);
  }

  public function delete(string $path, array $middlewares, $controller)
  {
    $route = new Route($path, "DELETE");

    foreach ($middlewares as $middleware)
    {
      $route->addToStack($middleware);
    }
    $route->addToStack($controller);
    array_push($this->routes, $route);
  }

  public function handle (Request $req, Response $res)
  {
    foreach ($this->stack as $middleware)
    {
      //TODO (RJ45): execute $middleware
      if ($middleware[0] === '#') //middleware
      {
        $middlewareObj = MiddlewareLoader::getMiddleware($middleware);
        if ($middlewareObj == null)
        {
          echo("Cannot find middleware $middleware");
          return;
        }
        $objSegments = explode('/', $middleware);
        $method = end($objSegments);
        $middlewareObj->execute($method, $req, $res);

        if ($res->isClosed()) {
          return;
        }
      }
    }

    foreach ($this->routes as $route)
    {
      if ($route->matchPath($req->path, $req->method))
      {
        $route->dispatch($req, $res);
        return;
      }
    }
    //TODO (RJ45): handle 404 error

    if ($req->method !== 'OPTIONS') {
      http_response_code(404);
      echo("Cannot $req->method $req->originalPath");
      exit();
    }
  }
}