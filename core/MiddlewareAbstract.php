<?php


namespace Core;


abstract class MiddlewareAbstract
{
  public function execute(string $methodName, Request &$req, Response &$res) {
    $this->{$methodName}($req, $res);
  }
}