<?php


namespace Core;


abstract class ControllerAbstract
{
  public function execute(string $methodName, Request &$req, Response &$res) {
    $this->{$methodName}($req, $res);
  }
}