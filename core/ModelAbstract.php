<?php


namespace Core;


abstract class ModelAbstract
{
  protected $connection;

  public function __construct($connection)
  {
    $this->connection = $connection;
  }
}