<?php


namespace App\Controllers;


use Core\ControllerAbstract;
use Core\Request;
use Core\Response;

class SetsController extends ControllerAbstract
{
  public function createSet (Request $req, Response $res)
  {
    $setId = $req->app->db->models['Set']->createSet($req->postData['name'], $req->jwtPayload->id);

    if ($setId == null)
      return $res->status(500)->end(json_encode(array('error' => 'Nie udało się utworzyć zestawu')));

    return $res->status(200)->end(json_encode(array('success' => 'ok', 'set_id' => $setId)));
  }

  public function getSet (Request $req, Response $res)
  {
    $onlyExpression = isset($req->query['onlyExpression']) && $req->query['onlyExpression'] == 1;

    $set = $req->app->db->models['Set']->getById($req->params['set_id'], $onlyExpression);

    if ($set == null)
      return $res->status(404)->end(json_encode(array('error' => 'Nie znaleziono zestawu')));

    return $res->status(200)->end(json_encode($set));
  }

  public function getAll (Request $req, Response $res)
  {
    $setsList = $req->app->db->models['Set']->getAll();

    if ($setsList == null)
      return $res->status(404)->end(json_encode(array('error' => 'Nie znaleziono żądnych zestawów do pokazania')));

    return $res->status(200)->end(json_encode(array(
      'sets' => $setsList
    )));
  }

  public function deleteSet (Request $req, Response $res)
  {
    $result = $req->app->db->models['Set']->deleteById($req->params['set_id'], $req->jwtPayload->id);

    if ($result == null)
      return $res->status(500)->end(json_encode(array('error' => 'Nie ma takiego zestawu, lub nie jesteś jego właścicielem')));

    return $res->status(200)->end(json_encode(array('success' => 'ok')));
  }

  public function addTranslationToSet (Request $req, Response $res)
  {
    $translationId = $req->app->db->models['Translation']->createTranslation($req->params['set_id'], $req->postData['english_word'], $req->postData['polish_word']);

    if ($translationId == null)
      return $res->status(500)->end(json_encode(array('error' => 'Nie udało się dodać tłumaczenia do zestawu')));

    return $res->status(200)->end(json_encode(array('success' => 'ok')));
  }
}