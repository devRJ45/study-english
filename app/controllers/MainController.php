<?php


namespace App\Controllers;


use Core\ControllerAbstract;
use Core\Request;
use Core\Response;

class MainController extends ControllerAbstract
{
  public function index (Request $req, Response $res)
  {
    $res->status(200)->end(json_encode(array('status' => 'ok')));
  }
}