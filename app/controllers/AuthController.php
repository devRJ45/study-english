<?php


namespace App\Controllers;


use App\Services\JWT;
use Core\ControllerAbstract;

class AuthController extends ControllerAbstract
{
  public function login ($req, $res)
  {
    $user = $req->app->db->models['User']->verifyLogin($req->postData['email'], $req->postData['password']);

    if ($user == null) {
      return $res->status(400)->end(
        json_encode(array(
          'error' => 'Niepoprawne dane logowania'
        ))
      );
    }

    $jwtPayload = array(
      'token_timestamp' => time(),
      'id' => $user['id'],
      'login' => $user['login']
    );

    $res->write(
      json_encode(array(
          'id' => $user['id'],
          'token' => JWT::encode($jwtPayload,'keykeykey')
      ))
    );
  }

  public function register ($req, $res)
  {
    $userId = $req->app->db->models['User']->register($req->postData['login'], $req->postData['email'], $req->postData['password']);

    if (!is_numeric($userId)) {
      return $res->status(400)->end(json_encode(array(
        "error" => $userId
      )));
    }

    $userId = (int)$userId;

    if (!is_integer($userId)) {
      return $res->status(400)->end(json_encode(array(
        "error" => "Nieznany błąd rejestracji"
      )));
    }

    $jwtPayload = array(
      'token_timestamp' => time(),
      'id' => $userId,
      'login' => $req->postData['login']
    );

    $res->write(
      json_encode(array(
        'id' => $userId,
        'token' => JWT::encode($jwtPayload,'keykeykey')
      ))
    );
  }

  public function showTokenInfo ($req, $res)
  {
    $res->write(
      json_encode($req->jwtPayload)
    );
  }
}