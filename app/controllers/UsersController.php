<?php


namespace App\Controllers;


use Core\ControllerAbstract;
use Core\Request;
use Core\Response;

class UsersController extends ControllerAbstract
{
  public function getActualUser (Request $req, Response $res)
  {
    $user = $req->app->db->models['User']->getById($req->jwtPayload->id);

    if ($user == null)
      return $res->status(404)->end(json_encode(array('error' => 'Nie znaleziono użytkownika')));

    return $res->status(200)->end(json_encode(array(
      'id' => $user['id'],
      'login' => $user['login']
    )));
  }

  public function getUser (Request $req, Response $res)
  {
    $user = $req->app->db->models['User']->getById($req->params['uid']);

    if ($user == null)
      return $res->status(404)->end(json_encode(array('error' => 'Nie znaleziono użytkownika')));

    return $res->status(200)->end(json_encode(array(
      'id' => $user['id'],
      'login' => $user['login']
    )));
  }

  public function getUserSets (Request $req, Response $res)
  {
    $userSets = $req->app->db->models['Set']->getUserSets($req->params['uid']);

    return $res->status(200)->end(json_encode(array(
      'sets' => $userSets
    )));
  }
}