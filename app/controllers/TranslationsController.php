<?php


namespace App\Controllers;


use Core\ControllerAbstract;
use Core\Request;
use Core\Response;

class TranslationsController extends ControllerAbstract
{
  public function deleteTranslation (Request $req, Response $res)
  {
    $result = $req->app->db->models['Translation']->deleteById($req->params['translation_id'], $req->jwtPayload->id);

    if ($result == null)
      return $res->status(500)->end(json_encode(array('error' => 'Nie ma takiego tłumaczenia, lub nie jesteś jego właścicielem')));

    return $res->status(200)->end(json_encode(array('success' => 'ok')));
  }
}