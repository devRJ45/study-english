<?php


namespace App\Routes;


use Core\Router;

class MainRouter extends Router
{
  public function __construct()
  {
    $this->initializeRoutes();
  }

  private function initializeRoutes ()
  {
    $this->use('/api/v1', new ApiRouter());
  }
}