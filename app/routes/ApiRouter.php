<?php


namespace App\Routes;


use App\Routes\Api\TranslationsRouter;
use App\Routes\Api\UsersRouter;
use App\Routes\Api\AuthRouter;
use App\Routes\Api\SetsRouter;
use Core\Router;

class ApiRouter extends Router
{
  public function __construct()
  {
    $this->initializeRoutes();
  }

  private function initializeRoutes ()
  {
    $this->use('/', "#AuthMiddleware/decodeToken");

    $this->get("/", array(), "@MainController/index");
    $this->get("/token", array('#AuthMiddleware/requireLogin'), "@AuthController/showTokenInfo");
    $this->use("/auth", new AuthRouter());
    $this->use("/user", new UsersRouter());
    $this->use("/set", new SetsRouter());
    $this->use("/translation", new TranslationsRouter());
    $this->get('/sets', array('#AuthMiddleware/requireLogin'), "@SetsController/getAll");
  }
}