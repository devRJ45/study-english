<?php


namespace App\Routes\Api;


use Core\Router;

class AuthRouter extends Router
{
  public function __construct()
  {
    $this->initializeRoutes();
  }

  private function initializeRoutes ()
  {
    $this->post("/login", array('#AuthMiddleware/validateLoginFields'), "@AuthController/login");
    $this->post("/register", array('#AuthMiddleware/validateRegisterFields'), "@AuthController/register");
  }
}