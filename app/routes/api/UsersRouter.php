<?php

namespace App\Routes\Api;

class UsersRouter extends \Core\Router
{
  public function __construct()
  {
    $this->initializeRoutes();
  }

  private function initializeRoutes ()
  {
//    $this->get('/', array(), "@UsersController/index");
    $this->get('/@me', array('#AuthMiddleware/requireLogin'), "@UsersController/getActualUser");
    $this->get('/:uid', array('#AuthMiddleware/requireLogin'), "@UsersController/getUser");
    $this->get('/:uid/sets', array('#AuthMiddleware/requireLogin'), "@UsersController/getUserSets");
  }
}