<?php


namespace App\Routes\Api;


class TranslationsRouter extends \Core\Router
{
  public function __construct()
  {
    $this->initializeRoutes();
  }

  private function initializeRoutes ()
  {
    $this->delete('/:translation_id', array('#AuthMiddleware/requireLogin'), "@TranslationsController/deleteTranslation");
  }
}