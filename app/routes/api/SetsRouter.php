<?php


namespace App\Routes\Api;


class SetsRouter extends \Core\Router
{
  public function __construct()
  {
    $this->initializeRoutes();
  }

  private function initializeRoutes ()
  {
    $this->post('/', array('#AuthMiddleware/requireLogin'), "@SetsController/createSet");
    $this->get('/:set_id', array('#AuthMiddleware/requireLogin'), "@SetsController/getSet");
    $this->post('/:set_id', array('#AuthMiddleware/requireLogin'), "@SetsController/addTranslationToSet");
    $this->delete('/:set_id', array('#AuthMiddleware/requireLogin'), "@SetsController/deleteSet");
  }
}