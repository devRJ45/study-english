<?php


namespace App\Models;

use PDO;
use Core\ModelAbstract;

class User extends ModelAbstract
{
  public function register (string $login, string $email, string $password) {
    if ($this->isEmailExist($email)) {
      return 'email istnieje w bazie';
    }

    $query = $this->connection->prepare("INSERT INTO `user` (`login`, `email`, `password`) VALUES (:login, :email, :password);");

    $data = array(
      'login' => $login,
      'email' => strtolower($email),
      'password' => $this->hashPassword($password)
    );

    $query->execute($data);

    return $this->connection->lastInsertId();
  }

  public function isEmailExist ($email)
  {
    $query = $this->connection->prepare("SELECT count(*) AS count FROM `user` WHERE `email` = :email LIMIT 1;");

    $data = array(
      'email' => strtolower($email)
    );

    $query->execute($data);

    if ($count = $query->fetch(PDO::FETCH_ASSOC))
    {
      return $count['count'] >= 1;
    }
    return false;
  }

  private function verifyPassword($hash, $password)
  {
    return password_verify($password, $hash);
  }

  private function hashPassword ($password)
  {
    $passwordOptions = array(
      'cost' => 12
    );
    return password_hash($password, PASSWORD_BCRYPT, $passwordOptions);
  }

  public function verifyLogin ($email, $password) {
    $query = $this->connection->prepare("SELECT `id`, `login`, `email`, `password` FROM `user` WHERE `email` = :email LIMIT 1;");

    $data = array(
      'email' => $email,
    );

    $query->execute($data);

    if ($user = $query->fetch(PDO::FETCH_ASSOC))
    {
      if ($this->verifyPassword($user['password'], $password)) {
        return array(
          'id' => $user['id'],
          'login' => $user['login']
        );
      }
    }
    return null;
  }

  public function getById ($userId)
  {
    $query = $this->connection->prepare("SELECT `id`, `login`, `email`, `password` FROM `user` WHERE `id` = :id LIMIT 1;");

    $data = array(
      'id' => $userId,
    );

    $query->execute($data);

    if ($user = $query->fetch(PDO::FETCH_ASSOC))
    {
      return array(
        'id' => $user['id'],
        'login' => $user['login']
      );
    }
    return null;
  }
}