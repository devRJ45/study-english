<?php


namespace App\Models;


use PDO;
use Core\ModelAbstract;

class Translation extends ModelAbstract
{
  public function createTranslation ($setId, $englishWord, $polishWord)
  {
    $this->connection->beginTransaction();

    $st = $this->connection->prepare("INSERT INTO english_word (expression, part_of_speech_id) VALUES (:english_word, 1)");
    $st->execute(array('english_word' => $englishWord));
    $englishWordId = $this->connection->lastInsertId();

    $st = $this->connection->prepare("INSERT INTO polish_word (expression, part_of_speech_id) VALUES (:polish_word, 1)");
    $st->execute(array('polish_word' => $polishWord));
    $polishWordId = $this->connection->lastInsertId();

    $st = $this->connection->prepare("INSERT INTO translation (english_word_id, polish_word_id) VALUES (:english_word_id, :polish_word_id)");
    $st->execute(array('english_word_id' => $englishWordId, 'polish_word_id' => $polishWordId));
    $translationId = $this->connection->lastInsertId();

    $st = $this->connection->prepare("INSERT INTO set_vocabulary (set_id, translation_id) VALUES (:set_id, :translation_id)");
    $st->execute(array('set_id' => $setId, 'translation_id' => $translationId));

    $this->connection->commit();

    return $this->connection->lastInsertId();
  }

  public function deleteById ($translationId, $ownerId)
  {
    $query = $this->connection->prepare("SELECT sv.id, t.id, pw.id, ew.id
                                            FROM n_set LEFT JOIN set_vocabulary sv ON n_set.id = sv.set_id 
                                            LEFT JOIN translation t on sv.translation_id = t.id 
                                            LEFT JOIN english_word ew on t.english_word_id = ew.id 
                                            LEFT JOIN polish_word pw on t.polish_word_id = pw.id 
                                            LEFT JOIN user_set on n_set.id = user_set.set_id
                                            WHERE t.id = :translation_id AND user_set.user_id = :user_id");
    $query->execute(array(
      'translation_id' => $translationId,
      'user_id' => $ownerId
    ));

    if ($translation = $query->fetch(PDO::FETCH_ASSOC)) {
      // exist! YAY!

      $query = $this->connection->prepare("DELETE FROM set_vocabulary WHERE translation_id = :translation_id");
      $query->execute(array(
        'translation_id' => $translationId,
      ));

      $query = $query = $this->connection->prepare("DELETE `t`, `pw`, `ew` FROM translation t LEFT JOIN english_word ew on t.english_word_id = ew.id LEFT JOIN polish_word pw on t.polish_word_id = pw.id WHERE t.id = :translation_id");
      $query->execute(array(
        'translation_id' => $translationId,
      ));

      return true;
    }

    return false;
  }
}