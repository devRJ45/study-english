<?php


namespace App\Models;


use Core\ModelAbstract;
use PDO;

class Set extends ModelAbstract
{
  public function getUserSets ($userId)
  {
    $query = $this->connection->prepare("SELECT n_set.id, n_set.name FROM user_set LEFT JOIN n_set ON user_set.set_id = n_set.id WHERE user_set.user_id = :user_id;");

    $data = array(
      'user_id' => $userId,
    );

    $query->execute($data);

    $userSets = array();

    while ($set = $query->fetch(PDO::FETCH_ASSOC))
    {
      $userSets[] = $set;
    }
    return $userSets;
  }

  public function getAll ()
  {
    $query = $this->connection->prepare("SELECT id, name FROM n_set");
    $query->execute();

    $setsList = array();

    while ($set = $query->fetch(PDO::FETCH_ASSOC))
    {
      $setsList[] = array(
        'id' => $set['id'],
        'name' => $set['name']
      );
    }

    return $setsList;
  }

  public function getById ($setId, $onlyExpression = false)
  {
    $query = $this->connection->prepare("SELECT n_set.name AS name, t.id as tid, ew.id AS ewid, ew.expression AS ewe, pw.id AS pwid, pw.expression AS pwe
                                            FROM n_set LEFT JOIN set_vocabulary sv ON n_set.id = sv.set_id 
                                            LEFT JOIN translation t on sv.translation_id = t.id 
                                            LEFT JOIN english_word ew on t.english_word_id = ew.id 
                                            LEFT JOIN polish_word pw on t.polish_word_id = pw.id 
                                            WHERE n_set.id = :set_id
                                            ORDER BY pw.expression ASC;");
    $data = array(
      'set_id' => $setId,
    );
    $query->execute($data);

    $setNotFound = true;

    $set = array(
      'translations' => array()
    );

    while ($translation = $query->fetch(PDO::FETCH_ASSOC))
    {
      $setNotFound = false;
      if (!$onlyExpression)
        $set['name'] = $translation['name'];

      if (is_null($translation['ewid']) || is_null($translation['pwid']))
        continue;

      if ($onlyExpression)
      {
        $set['translations'][] = array(
          'translation_id' => $translation['tid'],
          'english_word' => $translation['ewe'],
          'polish_word' => $translation['pwe'],
        );
      }
      else
      {
        $set['translations'][] = array(
          'translation_id' => $translation['tid'],
          'english_word' => array(
            'id' => $translation['ewid'],
            'expression' => $translation['ewe']
          ),
          'polish_word' => array(
            'id' => $translation['pwid'],
            'expression' => $translation['pwe']
          )
        );
      }
    }

    if ($setNotFound)
      return null;

    return $set;
  }

  public function createSet ($setName, $ownerId)
  {
    $this->connection->beginTransaction();

    $st = $this->connection->prepare("INSERT INTO n_set (name) VALUES (:set_name)");
    $st->execute(array('set_name' => $setName));
    $setId = $this->connection->lastInsertId();

    $st = $this->connection->prepare("INSERT INTO user_set (set_id, user_id) VALUES (:set_id, :user_id)");
    $st->execute(array(
      'set_id' => $setId,
      'user_id' => $ownerId
    ));

    $this->connection->commit();

    return $setId;
  }

  public function deleteById ($setId, $ownerId)
  {
    $this->connection->beginTransaction();

    $query = $this->connection->prepare("DELETE `t`, `sv`, `ew`, `pw`
                                            FROM n_set LEFT JOIN set_vocabulary sv ON n_set.id = sv.set_id 
                                            LEFT JOIN translation t on sv.translation_id = t.id 
                                            LEFT JOIN english_word ew on t.english_word_id = ew.id 
                                            LEFT JOIN polish_word pw on t.polish_word_id = pw.id 
                                            LEFT JOIN user_set on n_set.id = user_set.set_id
                                            WHERE n_set.id = :set_id AND user_set.user_id = :user_id");
    $query->execute(array(
      'set_id' => $setId,
      'user_id' => $ownerId
    ));

    $query = $this->connection->prepare("DELETE FROM user_set
                                            WHERE user_set.set_id = :set_id AND user_set.user_id = :user_id");
    $query->execute(array(
      'set_id' => $setId,
      'user_id' => $ownerId
    ));

    $query = $this->connection->prepare("DELETE FROM n_set
                                            WHERE n_set.id = :set_id");
    $query->execute(array(
      'set_id' => $setId,
    ));

    $this->connection->commit();

    return true;
  }
}