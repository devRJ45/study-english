<?php


namespace App\Services;


class JWT
{
  public static function decode ($token, $key)
  {
    $token = urldecode($token);
    $tokens = explode('.', $token);

    if (count($tokens) != 3)
      return null;

    $head = $tokens[0];
    $payload = $tokens[1];
    $sign = $tokens[2];

    $head = json_decode(base64_decode($head));
    $payload = json_decode(base64_decode($payload));

    if ($sign !== JWT::sign($head, $payload, $key))
      return null;

    return $payload;
  }

  public static function encode ($payload, $key)
  {
    $head = array('typ' => 'JWT', 'alg' => 'H256');

    $sign = JWT::sign($head, $payload, $key);

    $head = base64_encode(json_encode($head));
    $payload = base64_encode(json_encode($payload));

    return urlencode("$head.$payload.$sign");
  }

  public static function sign ($head, $payload, $key)
  {
    $head = base64_encode(json_encode($head));
    $payload = base64_encode(json_encode($payload));

    return base64_encode(hash_hmac('sha256', "$head.$payload", $key, true));
  }
}