<?php


namespace App\Services;

use PDO;
use App\Models\Set;
use App\Models\Translation;
use App\Models\User;

class Database
{
  private $connection;
  public $models = array();

  public function __construct($host = 'localhost', $dbname, $user, $password)
  {
    $this->connection = new PDO("mysql:host=$host;dbname=$dbname", $user, $password);
    $this->connection->query('SET NAMES utf8');
    $this->connection->query('SET CHARACTER_SET utf8_unicode_ci');

    $this->loadModels();
  }

  private function loadModels ()
  {
    $this->models['User'] = new User($this->connection);
    $this->models['Set'] = new Set($this->connection);
    $this->models['Translation'] = new Translation($this->connection);
  }
}