<?php


namespace App\Middlewares;


use App\Services\JWT;
use Core\MiddlewareAbstract;
use Core\Request;
use Core\Response;

class AuthMiddleware extends MiddlewareAbstract
{
  public function decodeToken (Request $req, Response $res)
  {
    if (!isset($req->headers['Authorization']))
      $req->jwtPayload = null;
    else
      $req->jwtPayload = JWT::decode($req->headers['Authorization'], "keykeykey");
  }

  public function requireLogin (Request $req, Response $res)
  {
    if (!isset($req->headers['Authorization']) || strlen($req->headers['Authorization']) <= 0)
      return $res->status(401)->end(json_encode(array(
        'error' => 'Wymagana autoryzacja'
      )));

    if (!isset($req->jwtPayload->id) || !isset($req->jwtPayload->login) || !isset($req->jwtPayload->token_timestamp))
      return $res->status(401)->end(json_encode(array(
        'error' => 'Token autoryzacyjny jest niepoprany'
      )));
  }

  public function validateLoginFields (Request $req, Response $res)
  {
    if (!isset($req->postData['email']) || !isset($req->postData['password']))
      return $res->status(400)->end(json_encode(array(
        "error" => "Nie wszystkie pola są wypełnione"
      )));
  }

  public function validateRegisterFields (Request $req, Response $res)
  {
    if (!isset($req->postData['login']) || !isset($req->postData['email']) || !isset($req->postData['password']))
      return $res->status(400)->end(json_encode(array(
        "error" => "Nie wszystkie pola są wypełnione"
      )));

    if (strlen($req->postData['login']) <= 3)
      return $res->status(400)->end(json_encode(array(
        "error" => "Login musi mieć przynajmniej 3 znaki"
      )));

    if (!filter_var($req->postData['email'], FILTER_VALIDATE_EMAIL))
      return $res->status(400)->end(json_encode(array(
        "error" => "Email nie jest poprawny"
      )));

    if (strlen($req->postData['login']) <= 3)
      return $res->status(400)->end(json_encode(array(
        "error" => "Hasło musi mieć przynajmniej 3 znaki"
      )));
  }
}