<?php


namespace App;


use Core\ControllerAbstract;

class ControllerLoader extends ControllerAbstract
{
  //TODO (RJ45): fix that
  public static function getController ($controller)
  {
    if ($controller[0] !== '@')
      return null;

    $controller = substr($controller, 1);

    $controllerSegments = explode('/', $controller);

    $method = array_pop($controllerSegments);
    $className = array_pop($controllerSegments);

    $class = "\\app\\controllers\\$className";

    return new $class;
  }
}