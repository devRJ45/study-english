<?php


namespace App;


use Core\MiddlewareAbstract;

class MiddlewareLoader extends MiddlewareAbstract
{
  //TODO (RJ45): fix that
  public static function getMiddleware ($middleware)
  {
    if ($middleware[0] !== '#')
      return null;

    $middleware = substr($middleware, 1);

    $middlewareSegments = explode('/', $middleware);

    $method = array_pop($middlewareSegments);
    $className = array_pop($middlewareSegments);

    $class = "\\app\\middlewares\\$className";

    return new $class;
  }
}