<?php


namespace App;

use App\Routes\MainRouter;
use App\Services\Database;
use Core\AppAbstract;


class App extends AppAbstract
{

  public $db;

  public function __construct()
  {
    global $config;
    parent::__construct();

    $this->router = new MainRouter();
    $this->db = new Database($config['db']['host'], $config['db']['db_name'], $config['db']['user'], $config['db']['password']);
  }
}